package com.example.tinysdk

import android.content.Context
import android.net.Uri
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.util.MimeTypes

class DashPlayer(context: Context, url: String) {

    private val player: SimpleExoPlayer = SimpleExoPlayer.Builder(context).build()

    init {
        player.setMediaItem(MediaItem.fromUri(url))
        player.prepare()
    }

    fun getExoPlayer(): SimpleExoPlayer {
        return player
    }

    fun play() {
        player.playWhenReady = true
    }

    fun pause() {
        player.playWhenReady = false
    }
}