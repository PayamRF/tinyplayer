package com.example.tinysdk

import android.content.Context
import android.util.AttributeSet
import com.google.android.exoplayer2.ui.PlayerView

class DashPlayerView(context: Context, attrs: AttributeSet) : PlayerView(context, attrs) {

    var dashPlayer: DashPlayer? = null
        set(value) {
            field = value
            player = value!!.getExoPlayer()
        }
}