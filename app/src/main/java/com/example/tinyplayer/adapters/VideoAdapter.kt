package com.example.tinyplayer.adapters

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tinyplayer.R
import com.example.tinyplayer.activities.MainActivity
import com.example.tinyplayer.activities.PlayerActivity
import com.example.tinyplayer.models.Video

class VideoAdapter(private val data: ArrayList<Video>, private val activity: Activity)
    : RecyclerView.Adapter<VideoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycle_video, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mData = data[position]
        holder.tvVideoTitle.text = mData.title
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvVideoTitle: TextView = itemView.findViewById(R.id.tvVideoTitle)

        init {
            itemView.setOnClickListener {
                val mData = data[adapterPosition]
                val intent = Intent(activity, PlayerActivity::class.java)
                intent.putExtra("videoTitle", mData.title)
                intent.putExtra("videoUrl", mData.url)
                activity.startActivity(intent)
            }
        }
    }
}