package com.example.tinyplayer.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tinyplayer.R
import com.example.tinyplayer.adapters.VideoAdapter
import com.example.tinyplayer.databinding.ActivityMainBinding
import com.example.tinyplayer.viewModels.MainActivityViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        initial()
    }

    private fun initial() {
        initNumber()
        viewModel.refreshVideoData()
    }

    private fun initNumber() {

        viewModel.videoData.observe(this, Observer {
            val adapter = VideoAdapter(it, this)
            binding.rvVideo.adapter = adapter
        })

        val layoutManager = LinearLayoutManager(this)
        binding.rvVideo.layoutManager = layoutManager
    }
}