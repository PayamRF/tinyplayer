package com.example.tinyplayer.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.tinyplayer.R
import com.example.tinyplayer.adapters.VideoAdapter
import com.example.tinyplayer.databinding.ActivityPlayerBinding
import com.example.tinyplayer.viewModels.PlayerActivityViewModel
import com.example.tinysdk.DashPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer

class PlayerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPlayerBinding
    private lateinit var viewModel: PlayerActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(PlayerActivityViewModel::class.java)

        initial()
    }

    private fun initial() {

        val videoTitle = intent.getStringExtra("videoTitle")
        val videoUrl = intent.getStringExtra("videoUrl")

        binding.tvVideoTitle.text = videoTitle

        viewModel.dashPlayer.observe(this, Observer {
            binding.dvVideo.dashPlayer = it
        })

        viewModel.initPlayer(this, videoUrl!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.pausePlayer()
    }
}