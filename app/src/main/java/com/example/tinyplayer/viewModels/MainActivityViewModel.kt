package com.example.tinyplayer.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tinyplayer.models.Video

class MainActivityViewModel : ViewModel() {

    var videoData = MutableLiveData<ArrayList<Video>>()

    init {
        videoData.value = ArrayList()
    }

    fun refreshVideoData() {
        val data = ArrayList<Video>()

        // Add videos
        data.add(Video("Video 1","http://dash.edgesuite.net/envivio/dashpr/clear/Manifest.mpd"))
        data.add(Video("Video 2","https://storage.googleapis.com/wvmedia/clear/h264/tears/tears.mpd"))
        data.add(Video("Video 3","https://storage.googleapis.com/exoplayer-test-media-0/dash-multiple-base-urls/manifest.mpd"))

        videoData.value = data
    }
}