package com.example.tinyplayer.viewModels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tinysdk.DashPlayer

class PlayerActivityViewModel : ViewModel()  {

    var dashPlayer = MutableLiveData<DashPlayer>()

    fun initPlayer(context: Context, url: String) {
        val player = DashPlayer(context, url)
        player.play()
        dashPlayer.value = player
    }

    fun pausePlayer() {
        val player = dashPlayer.value
        player?.pause()
    }
}